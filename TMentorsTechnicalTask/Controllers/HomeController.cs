﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TMentorsTechnicalTask.Models;
using System.Data.Entity;
using Newtonsoft.Json;

namespace TMentorsTechnicalTask.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationContext db = new ApplicationContext();

        // Get /
        // Get /home
        // Get /home/index
        public ActionResult Index()
        {
            var members = db.Members.Include(m => m.Project);
            return View(members.ToList());
        }
        
        // Get /home/Members
        // returns members data as json object
        public ActionResult Members()
        {
            var members = db.Members.Include(m => m.Project).Select(m => new
            {
                name = m.Name,
                project = new { name = m.Project.Name, id = m.Project.Id},
                title = m.Title
            }).ToList();

            return Json(members, JsonRequestBehavior.AllowGet);
        }


        // Get /home/projects
        // returns projects as a json object
        public ActionResult Projects()
        {
            var projects = db.Projects.Select(p => new
            {
                id = p.Id,
                name = p.Name
            });

            return Json(projects, JsonRequestBehavior.AllowGet);
        }


        // POST /home/AddMember
        // adds a new member to the database using the parameters given
        [HttpPost]
        public ActionResult AddMember([Bind(Include = "Name,Title,projectId")]Member member)
        {
            if (ModelState.IsValid)
            {
                db.Members.Add(member);
                db.SaveChanges();

                // return success
                return Json(new { result = "success" });
            }
            else // failure
                return Json(new { result = "failed" });

        }

        // POST /home/DeleteMember
        // deletes a member from the database using the given name and project id
        [HttpPost]
        public ActionResult DeleteMember(string name, int projectId)
        {
            try
            {
                var Member = db.Members.Where(m => m.Name.ToLower().Trim() == name.ToLower().Trim() && m.projectId == projectId).FirstOrDefault();

                if (Member != null)
                {
                    // delete member
                    db.Members.Remove(Member);
                    db.SaveChanges();
                }

                // return success
                return Json(new { result = "success" });
            }
            catch (Exception)
            {
                return Json(new { result = "failed" });
            }


        }

        // POST /home/UpdateMember
        // edits member's information in the database
        [HttpPost]
        public ActionResult UpdateMember(string oldName, int oldProjectId, [Bind(Include = "Name,Title,projectId")] Member newMember)
        {
            try
            {
                // find member to delete
                var member = db.Members.Where(m => m.Name.Trim().ToLower() == oldName.Trim().ToLower() && m.projectId == oldProjectId).FirstOrDefault();

                if(member != null)
                {
                    member.Name = newMember.Name;
                    member.Title = newMember.Title;
                    member.projectId = newMember.projectId;
                }

                // save changes
                db.SaveChanges();

                // return success
                return Json(new { result = "success" });
            }
            catch(Exception)
            {
                // return failure
                return Json(new { result = "failure" });
            }
        }

    }
}