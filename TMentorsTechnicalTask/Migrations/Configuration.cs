namespace TMentorsTechnicalTask.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using TMentorsTechnicalTask.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<TMentorsTechnicalTask.Models.ApplicationContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "TMentorsTechnicalTask.Models.ApplicationContext";
        }

        protected override void Seed(TMentorsTechnicalTask.Models.ApplicationContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
                //context.People.AddOrUpdate(
                //  p => p.FullName,
                //  new Person { FullName = "Andrew Peters" },
                //  new Person { FullName = "Brice Lambson" },
                //  new Person { FullName = "Rowan Miller" }
                //);
            //

            context.Projects.AddOrUpdate(
              p => p.Name,
              new Project { Name = "Project 1", StartDate = DateTime.Now, EndDate = DateTime.Now.AddMonths(1) },
              new Project { Name = "Project 2", StartDate = DateTime.Now, EndDate = DateTime.Now.AddMonths(1) },
              new Project { Name = "Project 3", StartDate = DateTime.Now, EndDate = DateTime.Now.AddMonths(1) }
            );
        }
    }
}
