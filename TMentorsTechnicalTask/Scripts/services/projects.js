﻿app.factory('projects', ['$http', function ($http) {

    return $http.get('home/projects')
            .success(function (data) {
                return data;
            })
            .error(function (data) {
                return data;
            })

}]);