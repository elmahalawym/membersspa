﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TMentorsTechnicalTask.Migrations;

namespace TMentorsTechnicalTask.Models
{
    public class ApplicationContext : DbContext
    {
        static ApplicationContext()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationContext, Configuration>());
        }

        public DbSet<Project> Projects { get; set; }

        public DbSet<Member> Members { get; set; }
    }
}