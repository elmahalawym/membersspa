﻿app.factory('members', ['$http', function ($http) {

    return $http.get('home/members')
            .success(function (data) {
                return data;
            })
            .error(function (data){
                return data;
            })

}]);