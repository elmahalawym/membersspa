﻿app.controller('MainController', ['$scope', 'members', 'projects', '$http', 
function ($scope, members, projects, $http) {
    $scope.title = "TMentors Technical Test";
    $scope.selectedProjectId = 0;
    $scope.deleteMemberName = "";
    $scope.memberToDelete = {};
    $scope.memberToEdit = {};

    //$scope.members = 
    //    [
    //        {
    //            project: "project 1",
    //            name: "ahmed",
    //            title: "Mr"
    //        },
    //        {
    //            project: "project 2",
    //            name: "ibrahim",
    //            title: "Mr"
    //        }, {
    //            project: "project 3",
    //            name: "yaser",
    //            title: "Mr"
    //        }, {
    //            project: "project 1",
    //            name: "heba",
    //            title: "Mrs"
    //        },
    //    ]

    members.success(function (data) {
        $scope.allMembers = data;
        $scope.members = $scope.allMembers.slice();
        $scope.selectedProjectId = 0;
    });

    projects.success(function (data) {
        $scope.projects = data;
    });

    $scope.filterMembers = function (selectedProject) {
        console.log(selectedProject);
    };

    $scope.projectClicked = function (selected) {
        console.log('project changed');
        console.log(selected);
        if (!selected) {
            $scope.members = $scope.allMembers.slice();
        }
        else {
            $scope.members = [];
            console.log($scope.allMembers);
            for (var i = 0; i < $scope.allMembers.length; i++) {
                if ($scope.allMembers[i].project.name === selected.name) {
                    console.log($scope.allMembers[i].project + ' ' + selected.name);
                    $scope.members.push($scope.allMembers[i]);

                }
            }
        }

    }

    $scope.AddMember = function () {
        //console.log($scope.addMember_name);
        //console.log($scope.addMember_title);
        //console.log($scope.addMember_project);
        $http.post('home/AddMember', { Name: $scope.addMember_name, Title: $scope.addMember_title, projectId: $scope.addMember_project.id })
        .success(function (data) {
            console.log(data);
            // add member to collection
            $scope.allMembers.push({
                project: $scope.addMember_project,
                name: $scope.addMember_name,
                title: $scope.addMember_title
            });

            // reselect all projects
            $scope.selectedProjectId = 0;
            $scope.members = $scope.allMembers.slice();
            
            // hide modal
            $('#createMemberModal').modal('hide');
        });

    };


    $scope.deleteMemberClicked = function ($index) {
        $scope.deleteMemberName = $scope.members[$index].name;
        $scope.memberToDelete = $scope.members[$index];
        console.log($scope.memberToDelete);
        //console.log($scope.deleteMemberName);
    };

    $scope.deleteMember = function() {
        $http.post('home/DeleteMember', { name: $scope.memberToDelete.name, projectId: $scope.memberToDelete.project.id })
            .success(function (data) {
                console.log(data);

                // remove member
                var index = $scope.allMembers.indexOf($scope.memberToDelete);
                if (index > -1)
                    $scope.allMembers.splice(index, 1);

                // reselect all projects
                $scope.selectedProjectId = 0;
                $scope.members = $scope.allMembers.slice();

                // hide modal
                $('#deleteModal').modal('hide');
            });
    }

    $scope.editMemberClicked = function ($index) {
        $scope.memberToEdit = $scope.members[$index];
        
        $scope.editMember_name = $scope.memberToEdit.name;
        $scope.editMember_project = $scope.memberToEdit.project.id;
        $scope.editMember_title = $scope.memberToEdit.title;
        
    }

    $scope.editMember = function () {

        $http.post('home/updateMember',
            {
                oldName: $scope.memberToEdit.name,
                oldProjectId: $scope.memberToEdit.project.id,
                Name: $scope.editMember_name,
                Title: $scope.editMember_title,
                projectId: $scope.editMember_project
            })
            .success(function (data) {
                // test
                console.log(data);

                // edit current data
                $scope.memberToEdit.name = $scope.editMember_name;
                $scope.memberToEdit.project.id = $scope.editMember_project;
                $scope.memberToEdit.project.name = 'Project ' + $scope.editMember_project;
                $scope.memberToEdit.title = $scope.editMember_title;

                // hide modal
                $('#editMemberModal').modal('hide');
            });

    };


}]);