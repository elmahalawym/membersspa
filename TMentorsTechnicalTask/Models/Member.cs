﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TMentorsTechnicalTask.Models
{
    public class Member
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Title { get; set; }

        [ForeignKey("Project")]
        public int projectId { get; set; }
        public virtual Project Project { get; set; }
    }
}